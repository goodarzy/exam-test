import React from 'react';
import {
    View,
    Text,
    ActivityIndicator,
    Image,
    ImageBackground,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    Keyboard,
    FlatList,
    StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";

export default class ActorDetail extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [];
    }

    state = {
        UserToken: null,
        ActorsName: null,
        ActorsDob: null,
        ActorsImage: null,
        loading: true,
        ID: null
    };

    componentDidMount() {
        const id = this.props.navigation.getParam('ActorsID');
        this.setState({ ID: id })
        this.getToken()
            .then(() => {
                this.getActorsDetail()
            })
    }

    async getToken() {
        // AsyncStorage.removeItem('UserToken');
        const userToken = await AsyncStorage.getItem('UserToken');
        // console.warn(userToken)
        this.setState({ UserToken: userToken })
    }

    async getActorsDetail() {
        fetch(`https://halloffame-server.herokuapp.com/fames/${this.state.ID}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.state.UserToken}`
            },
        })
            .then((response) => response.json())
            .catch((error) => alert('E1'))
            .then((res) => {
                // console.warn(JSON.stringify(res))
                const Data = res.data
                this.setState({
                    ActorsName: Data.name,
                    ActorsDob: Data.dob,
                    ActorsImage: Data.image,
                    loading: false
                })
            }).catch((error) => alert('E2'))
            .done();
    }

    render() {
        return (
            <View style={styles.mainContent}>
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor="#ffffff" translucent={true} />
                <View style={styles.main}>
                    <TouchableOpacity style={styles.headerLeft}
                        onPress={() =>
                            // this.props.navigation.navigate("Manage")
                            this.props.navigation.goBack(null)
                        }>
                        <View style={styles.headerButtonLeft}>
                            <Icon name="chevron-left" size={20} color={'#ffffff'} />
                        </View>
                    </TouchableOpacity>
                    <View style={styles.headerBody}>
                        <Text style={styles.headerText}>Detail</Text>
                    </View>
                    <TouchableOpacity style={styles.headerRight}>
                    </TouchableOpacity>
                </View>
                <View style={styles.flatListView}>
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                        <Image style={styles.FLImage}
                            source={{ uri: this.state.ActorsImage }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={styles.FLText}>{this.state.ActorsName}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={styles.FLText}>{this.state.ActorsDob}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.buttonView}>
                    <TouchableOpacity onPress={() => {
                        AsyncStorage.removeItem('UserToken');
                        this.props.navigation.navigate("Login");
                    }}>
                        <Text style={styles.BText}>LogOut</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    flatListView: {
        width: '100%',
        height: '78%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonView: {
        width: '100%',
        height: '10%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'gray',
    },
    BText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
    },
    flatList: {
        width: '90%',
        height: 75,
        backgroundColor: 'lightgray',
        alignSelf: 'center',
        borderRadius: 28,
        flexDirection: 'row',
        marginVertical: '2%'
    },
    FLImage: {
        width: 250,
        height: 250,
        borderRadius: 22,
        alignSelf: 'center',
        marginLeft: '2%',
        borderWidth: 1,
        borderColor: '#F2F3F5'
    },
    FLText: {
        color: 'black',
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'roboto',
        marginLeft: '5%',
        alignSelf: 'center'
    },
    Bcreat: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: 50,
        backgroundColor: '#1455CC',
        borderRadius: 25
    },
    emptyList: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: 60,
        backgroundColor: '#ffffff',
        borderRadius: 25,
        alignSelf: 'center'
    },
    indicator: {
        backgroundColor: '#F2F3F5',
        width: '100%',
        height: '100%',
    },
    main: {
        flexDirection: 'row',
        width: '100%',
        height: 60,
        backgroundColor: '#1250C1',
        justifyContent: 'center'
    },
    headerLeft: {
        width: '15%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerBody: {
        width: '70%',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    headerRight: {
        width: '15%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerText: {
        color: '#ffffff',
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'roboto',
    },
    headerButtonRight: {
        justifyContent: 'center',
        alignSelf: 'flex-end',
        marginRight: 20
    },
    headerButtonLeft: {
        justifyContent: 'center',
        alignSelf: 'flex-start',
        marginLeft: 20
    },
});
