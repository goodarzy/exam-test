import React from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  Image,
  ImageBackground,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  Keyboard,
  Alert,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import AsyncStorage from '@react-native-community/async-storage';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    isModalVisible: false,
    BTerm: true,
    UserToken: '',
    loginState: '',
    UserState: null,
    loading: false,
    email: null,
    password: null,
  };

  componentDidMount() {
    this.checkAPIHealth()
  }

  componentDidUpdate() {

  }

  TermofUse() {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    })
  }

  TermofUseB() {
    this.setState({
      BTerm: !this.state.BTerm,
    })
  }

  checkAPIHealth() {
    fetch('https://halloffame-server.herokuapp.com/health', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json()).catch((error) => alert('E1'))
      .then((res) => {
        // console.warn(JSON.stringify(res))
      }).catch((error) => alert('E2'))
      .done();
  }

  Login() {
    Keyboard.dismiss()

    fetch('https://halloffame-server.herokuapp.com/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer JWTToken'
      },
      body: JSON.stringify({
        username: this.state.email,
        password: this.state.password
      })
    })
      .then((response) => {
        // console.warn(response.headers.map.authorization)
        // console.warn(response.status)
        const userToken = response.headers.map.authorization
        AsyncStorage.setItem('UserToken', userToken)
        this.setState({UserToken: userToken})
        if(response.status == 200){
          this.props.navigation.navigate("Home")
        }
      })
      .catch((error) => alert('E1'))
      .done();
  }

  modalShow() {
    return (
      <Modal
        isVisible={this.state.isModalVisible}
        animationIn="slideInLeft"
        animationOut="slideOutRight"
        onBackdropPress={() => this.setState({ isModalVisible: !this.state.isModalVisible })}
        style={{ justifyContent: 'center', alignContent: 'center' }}
      >
        <View style={styles.modalView}>
          <View style={styles.modalHeader}>
            <Text style={styles.modalTitle}>Term of Use</Text>
          </View>
          <View style={styles.modalBody}>
            <ScrollView style={styles.modalSView}>
              <Text style={styles.modalText}>By downloading, browsing, accessing or using mobile application (“Mobile Application”).</Text>
            </ScrollView>
          </View>
          <View style={styles.modalFotter}>
            <TouchableOpacity style={styles.bD}
              onPress={() => {
                this.setState({
                  isModalVisible: !this.state.isModalVisible,
                })
              }}>
              <Text style={styles.bDText}>Done</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  render() {
    if (this.state.loading == true) {
      return (
        <ActivityIndicator size='large' color='#1455CC' style={styles.indicator} />
      )
    } else {
      return (
        <View style={styles.mainContent}>
          <View style={styles.card}>
            <StatusBar hidden />
            <View style={styles.bP}>
              <View style={styles.bVIcon}>
                <TextInput style={styles.textInput}
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                  placeholder='Type your email'
                  keyboardType="email-address"
                />
              </View>
            </View>
            <View style={styles.bE}>
              <View style={styles.bVIcon}>
                <TextInput style={styles.textInput}
                  onChangeText={password => this.setState({ password })}
                  value={this.state.password}
                  secureTextEntry={true}
                  placeholder='Type your password'
                  keyboardType="default"
                />
              </View>
            </View>
            <TouchableOpacity style={styles.bG}
              onPress={() => {
                if (this.state.BTerm == false) {
                  alert('You should ticked Term of Use first')
                } else {
                  this.Login()
                }
              }}>
              <View style={styles.bVIcon}>
                <Text style={styles.bVText}>Login</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.bT}
              onPress={() => { this.TermofUseB() }}>
              <View style={styles.bVTImage}>
                <Image style={styles.termImage} resizeMethod={'resize'}
                  source={(this.state.BTerm == false) ? require('../Assets/icons/termsDisactive.png') : require('../Assets/icons/termsActive.png')} />
              </View>
              <View style={styles.bVTText}>
                <Text style={styles.bTText}
                  onPress={() => { this.TermofUse() }}>Term of Use</Text>
              </View>
            </TouchableOpacity>
            {this.modalShow()}
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    backgroundColor: '#F1F3F4',
    alignItems: 'center',
  },
  card: {
    position: 'absolute',
    zIndex: 2,
    backgroundColor: '#ffffff',
    height: 300,
    width: '88%',
    marginTop: '30%',
    borderRadius: 30,
    alignItems: 'center',
  },
  bP: {
    backgroundColor: '#f3f3f3',
    height: '16%',
    width: '85%',
    borderRadius: 14,
    marginTop: 30,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  bE: {
    backgroundColor: '#f3f3f3',
    height: '16%',
    width: '85%',
    borderRadius: 14,
    marginTop: 20,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  bG: {
    backgroundColor: '#f3f3f3',
    height: '16%',
    width: '85%',
    borderRadius: 22,
    marginTop: 20,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  bT: {
    backgroundColor: '#ffffff',
    height: '16%',
    width: '85%',
    borderRadius: 22,
    marginTop: 30,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  bVIcon: {
    backgroundColor: '#f3f3f3',
    height: '100%',
    width: '100%',
    borderRadius: 22,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bVText: {
    color: '#525252',
    fontSize: 18,
    textAlign: 'left',
    fontFamily: 'roboto',
  },
  textInput: {
    fontSize: 18,
    color: 'black',
    textAlign: 'center',
  },
  bD: {
    backgroundColor: '#1455CC',
    height: 40,
    width: 300,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bText: {
    color: '#525252',
    fontSize: 18,
    textAlign: 'left',
    fontFamily: 'roboto',
  },
  bTText: {
    color: '#1455CC',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'roboto',
    marginRight: '40%',
    textDecorationLine: 'underline'
  },
  bDText: {
    color: '#ffffff',
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'roboto',
    fontWeight: 'bold'
  },
  termImage: {
    height: 30,
    width: 30,
    marginLeft: '40%'
  },
  modalView: {
    height: '90%',
    width: '100%',
    backgroundColor: '#F1F3F4',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 35
  },
  modalHeader: {
    height: '10%',
    width: '90%',
    backgroundColor: '#F1F3F4',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 35
  },
  modalBody: {
    height: '75%',
    width: '80%',
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 35
  },
  modalSView: {
    width: '80%',
    marginTop: '2%'
  },
  modalFotter: {
    height: '15%',
    width: '90%',
    backgroundColor: '#F1F3F4',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 35
  },
  modalTitle: {
    color: '#1455CC',
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'roboto',
    fontWeight: 'bold'
  },
  modalText: {
    color: '#525252',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: 'roboto',
  },
  indicator: {
    backgroundColor: '#F2F3F5',
    width: '100%',
    height: '100%'
  },
});
