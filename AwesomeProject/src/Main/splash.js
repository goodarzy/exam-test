import React from 'react';
import {
    View,
    StatusBar,
    Text,
    ActivityIndicator,
    Image,
    ImageBackground,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import DeviceInfo from 'react-native-device-info';


export default class Splash extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        UserToken: null,
        Display: null
    }

    componentDidMount() {
        this.getToken()
        .then(() => {
            this.checkToken()
        })
        this.getVersion()
    }

    async getToken() {
        // AsyncStorage.removeItem('UserToken');
        const userToken = await AsyncStorage.getItem('UserToken');
        // console.warn(userToken)
        this.setState({ UserToken: userToken })
    }

    getVersion() {
        DeviceInfo.getDisplay().then(display => {
            // console.warn(display)
            this.setState({ Display: display })
        });
    }

    checkToken() {
        // console.warn(this.state.UserToken)
        if (this.state.UserToken == null) {
            this.timeoutHandle = setTimeout(() => {
                this.props.navigation.navigate("Login")
            }, 1000);
        } else {
            this.props.navigation.navigate("Home")
        }
    }

    render() {
        return (
            <View style={styles.mainContent}>
                <StatusBar hidden />
                <Image style={styles.image} source={require('../Assets/splash/splash.jpg')} resizeMode={"contain"} >
                </Image >
                <Text style={styles.text}>{this.state.Display}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        height: '90%',
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: 'black',
        backgroundColor: 'transparent',
        textAlign: 'justify',
        fontWeight: "bold",
        fontSize:18
    },
});