import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import React from 'react';
import { Image, TouchableWithoutFeedback } from 'react-native';

import Spalsh from "./splash";
import Login from "./login";
import Home from "./home";
import Detail from "./actorDetail"


// Main navigator
const RootStack = createStackNavigator({
    Spalsh: {
        screen: Spalsh,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },
    Home: {
        screen: Home,
        navigationOptions: {
            header: null
        }
    },
    Detail: {
        screen: Detail,
        navigationOptions: {
            header: null
        }
    },
});
const navigator = createAppContainer(RootStack);
// Export it as the root component
export default navigator

