import React from 'react';
import {
    View,
    Text,
    ActivityIndicator,
    Image,
    ImageBackground,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    Keyboard,
    FlatList,
    StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [];
    }

    state = {
        UserToken: null,
        ActorsList: [],
        ActorsID: [],
        ActorsDetail: [],
        loading: true
    };

    componentDidMount() {
        this.getToken()
            .then(() => {
                this.getActorsList()
            })
    }

    async getToken() {
        // AsyncStorage.removeItem('UserToken');
        const userToken = await AsyncStorage.getItem('UserToken');
        // console.warn(userToken)
        this.setState({ UserToken: userToken })
    }

    async getActorsList() {
        fetch('https://halloffame-server.herokuapp.com/fames?page=0', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.state.UserToken}`
            },
        })
            .then((response) => response.json())
            .catch((error) => alert('E1'))
            .then((res) => {
                // console.warn(JSON.stringify(res.data.list))
                this.setState({
                    ActorsList: res.data.list,
                    loading: false
                })
            }).catch((error) => alert('E2'))
            .done();
    }

    renderItem = data =>
        <TouchableOpacity style={styles.flatList}
            onPress={() => { this.selectItem(data) }}>
            <View style={styles.FLPerson}>
                <TouchableOpacity
                    onPress={() => {
                        const actorsID = data.item.id
                        // console.warn(actorsID)
                        this.props.navigation.navigate("Detail", { ActorsID: actorsID })
                    }}
                    style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                    <Image style={styles.FLImage}
                        source={{ uri: data.item.image }} />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        const actorsID = data.item.id
                        // console.warn(actorsID)
                        this.props.navigation.navigate("Detail", { ActorsID: actorsID })
                    }}>
                    <Text style={styles.FLText}>{data.item.name}</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.FLCheckbox}
                onPress={() => { this.selectItem(data) }}>
                <Image style={styles.checkboxImage} resizeMethod={'resize'}
                    source={(data.item.Selected == true) ? require('../Assets/icons/like.png') : require('../Assets/icons/unlike.png')} />
            </TouchableOpacity>
        </TouchableOpacity>

    selectItem = data => {
        // console.warn('Data:'+JSON.stringify(data))
        data.item.Selected = !data.item.Selected;
        // console.warn(data.item.Selected)
        const index = this.state.ActorsList.findIndex(
            item => data.item.id === item.id
        );
        //  console.warn('Index:'+ index)
        this.state.ActorsList[index] = data.item;

        var arrID = this.state.ActorsID
        var arrDetail = this.state.ActorsDetail
        if (data.item.Selected == true) {
            arrID.push(data.item.id);
            arrDetail.push(data.item)
            //  console.warn('1:'+arr)
            this.setState({
                ActorsList: this.state.ActorsList,
                ActorsID: this.state.ActorsID,
                ActorsDetail: this.state.ActorsDetail
            });
        } else {
            var Aindex = arrID.indexOf(data.item.id);
            var Bindex = arrDetail.indexOf(data.item)
            arrID.splice(Aindex, 1);
            arrDetail.splice(Bindex, 1);
        }
    };

    emptyList() {
        return (
            <View style={styles.emptyList}>
                <Text style={styles.ELText}>No match found</Text>
            </View>
        )
    }


    render() {
        const { ActorsList } = this.state;
        if (this.state.loading == true) {
            return (
                <ActivityIndicator size='large' color='#1455CC' style={styles.indicator} />
            )
        } else {
            return (
                <View style={styles.mainContent}>
                    <StatusBar  barStyle = "dark-content" hidden = {false} backgroundColor = "#ffffff" translucent = {true}/>
                    <View style={styles.main}>
                        <View style={styles.headerLeft} />
                        <View style={styles.headerBody}>
                            <Text style={styles.headerText}>Home</Text>
                        </View>
                        <TouchableOpacity style={styles.headerRight}>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.flatListView}>
                        <FlatList
                            data={ActorsList}
                            renderItem={item => this.renderItem(item)}
                            bounces={false}
                            onEndReachedThreshold={0.01}
                            keyExtractor={(item, index) => item + index.toString()}
                            ListEmptyComponent={this.emptyList()}
                        />
                    </View>
                    <View style={styles.buttonView}>
                        <TouchableOpacity onPress={() => {
                            AsyncStorage.removeItem('UserToken');
                            this.props.navigation.navigate("Login");
                        }}>
                            <Text style={styles.BText}>LogOut</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    flatListView: {
        width: '100%',
        height: '78%',
    },
    buttonView: {
        width: '100%',
        height: '10%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    BText: {
        color: 'black',
        fontSize: 20,
        textAlign: 'center',
    },
    flatList: {
        width: '90%',
        height: 75,
        backgroundColor: 'lightgray',
        alignSelf: 'center',
        borderRadius: 28,
        flexDirection: 'row',
        marginVertical: '2%'
    },
    FLImage: {
        width: 60,
        height: 60,
        borderRadius: 22,
        alignSelf: 'center',
        marginLeft: '2%',
        borderWidth: 1,
        borderColor: '#F2F3F5'
    },
    FLText: {
        color: 'black',
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'roboto',
        marginLeft: '5%',
        alignSelf: 'center'
    },
    FLCheckbox: {
        position: 'absolute',
        width: 75,
        height: 75,
        borderRadius: 22,
        left: '80%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkboxImage: {
        height: 35,
        width: 40
    },
    FLPerson: {
        flexDirection: 'row',
        width: '70%',
        height: 75,
        alignItems: 'center',
        marginLeft: '3%',
    },
    Bcreat: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: 50,
        backgroundColor: '#1455CC',
        borderRadius: 25
    },
    emptyList: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: 60,
        backgroundColor: '#ffffff',
        borderRadius: 25,
        alignSelf: 'center'
    },
    ELText: {
        color: 'black',
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'roboto',
    },
    indicator: {
        backgroundColor: '#F2F3F5',
        width: '100%',
        height: '100%',
    },
    main: {
        flexDirection: 'row',
        width: '100%',
        height: 60,
        backgroundColor: '#1250C1',
        justifyContent: 'center'
    },
    headerLeft: {
        width: '15%',
        justifyContent: 'center',
    },
    headerBody: {
        width: '70%',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    headerRight: {
        width: '15%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerText: {
        color: '#ffffff',
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'roboto',
    },
    headerButtonRight: {
        justifyContent: 'center',
        alignSelf: 'flex-end',
        marginRight: 20
    },
});
